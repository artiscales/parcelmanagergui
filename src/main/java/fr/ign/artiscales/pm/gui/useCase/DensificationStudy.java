package fr.ign.artiscales.pm.gui.useCase;

import fr.ign.artiscales.pm.gui.tools.Tools;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;

public class DensificationStudy extends UseCaseScene {

    static List<String> necessariesElements = Arrays.asList("rootFolder", "outputFolder");
    public DensificationStudy(Stage stageGoal) {
        super(stageGoal, necessariesElements);
        stageGoal.setTitle("Densification Study");
        getParameters(stageGoal);
    }

    public void run() {
        if (checkIfFilled())
            try {
                fr.ign.artiscales.pm.usecase.DensificationStudy.runDensificationStudy(rootFolder, outFolder);
            } catch (Exception e) {
                Tools.printEx(e);
                e.printStackTrace();
            }
    }
}
