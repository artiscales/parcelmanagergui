package fr.ign.artiscales.pm.gui.workflow;

import fr.ign.artiscales.pm.division.DivisionType;
import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.setParameters.GeneralOptions;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.pm.workflow.Workflow;
import fr.ign.artiscales.pm.workflow.ZoneDivision;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ZoneDivisionWorkflow extends WorkflowScene {
    static List<String> necessariesElements = new ArrayList<>(Arrays.asList("parcel", "zone", "profileUrbanFabric", "outputFolder"));
    static List<String> optionalElements = Collections.singletonList("road");
    public static boolean KEEPEXISTINGROADS;

    public ZoneDivisionWorkflow(Stage stage) {
        super(stage,"ZoneDivision", necessariesElements, optionalElements);
        stage.setTitle("Zone Division");
        getParameters(stage);
    }

    @Override
    public void run() {
        if (Workflow.PROCESS.equals(DivisionType.SS) || Workflow.PROCESS.equals(DivisionType.SSoffset) || Workflow.PROCESS.equals(DivisionType.OBBThenSS)) {
            necessariesElements.add("road");
            GeneralOptions.doGeneratePeripheralRoad();
        }
        if (checkIfFilled())
            try {
                // Task<Void> longRunningTask = new Task<Void>() {
                //
                // @Override
                // protected Void call() throws Exception {

//				Alert alert = new Alert(AlertType.INFORMATION);
//				alert.setContentText("Workflow <b>Consolidation Division</b> is currently processing. Please wait the new popup.");
//				alert.setHeaderText("In progress");
//				alert.show();
                CollecMgmt.exportSFC((new ZoneDivision()).zoneDivision(zoneFile, sfcParcel, roadFile, profile, KEEPEXISTINGROADS, outFolder),
                        Tools.getNewFileToSave(outFolder, "ZoneDivisionOutput" + App.geoFormat));
//				Alert alert2 = new Alert(AlertType.INFORMATION);
//				alert2.setContentText("Workflow has successfully proceeded. Find the result in the " + outFolder.getAbsolutePath() + " folder");
//				alert2.setHeaderText("Done");
//				alert2.show();
                // Platform.runLater(() -> status.setText("Connected"));
                // return null;
                // }
                // };
                // button.setOnAction(e -> {
                // new Thread(longRunningTask).start();
                // });
            } catch (Exception e) {
                Tools.printEx(e);
                e.printStackTrace();
            }
    }
}
