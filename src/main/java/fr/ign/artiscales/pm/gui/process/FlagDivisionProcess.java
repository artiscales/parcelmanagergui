package fr.ign.artiscales.pm.gui.process;

import fr.ign.artiscales.pm.division.FlagDivision;
import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.tools.CreateUrbanProfile;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

public class FlagDivisionProcess extends ProcessScene {

    public FlagDivisionProcess(Stage stage) {
        super(stage);
        stage.setTitle("Flag cut process");
        necessariesElements = Arrays.asList("parcel", "outputFolder");
        optionalElements = Collections.singletonList("road");
        VBox parcel = new VBox();
        parcel.getChildren().addAll(getButtonParcel(stage), new Label(parcelName));

        VBox road = new VBox();
        road.getChildren().addAll(getButtonRoad(stage), new Label("(optional)" + roadName));

        VBox building = new VBox();
        building.getChildren().addAll(getButtonBuilding(stage), new Label("(optional)" + buildingName));

        VBox outputFolder = new VBox();
        outputFolder.getChildren().addAll(getOutFolderButton(stage), new Label(outFolder != null ? outFolder.getName() : ""));

        final String process = "flagDivision";
        CreateUrbanProfile cub = new CreateUrbanProfile(process);
        VBox params = cub.getUrbanProfileParams(process);
        cub.setNeededFields(process);

        Button importPUF = cub.buttonImport(stage);

        Button runButton = new Button("Run");
        runButton.setOnAction(e -> {
            if (cub.getConfirmationAction() && checkIfFilled()) {
                this.profile = cub.getProfile();
                try {
                    CollecMgmt.exportSFC(FlagDivision.doFlagDivision(sfcParcel, buildingFile, roadFile, profile), Tools.getNewFileToSave(outFolder, "FlagSplitParcel"+ App.geoFormat));
                    Tools.getProcessAlert("FlagCut process has successfully been proceeded. Find the result in the " + outFolder.getAbsolutePath() + " folder","Done");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
        rootGroup.getChildren().addAll(new Label("Geographic data to import"), parcel, road, building, outputFolder, params, importPUF, runButton);
    }
}
