package fr.ign.artiscales.pm.gui.setParameters;

import fr.ign.artiscales.pm.division.StraightSkeletonDivision;
import fr.ign.artiscales.pm.gui.workflow.DensificationWorkflow;
import fr.ign.artiscales.pm.gui.workflow.ZoneDivisionWorkflow;
import fr.ign.artiscales.pm.workflow.Workflow;
import fr.ign.artiscales.pm.division.DivisionType;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class GeneralOptions {

    public static VBox getSetProcess() {
        // ChoiceBox<String> dialog = new ChoiceBox<>( FXCollections.observableArrayList("Oriented Bounding Box", "Straight Skeleton", "Median Skeleton"));
        ChoiceBox<String> dialog = new ChoiceBox<>(FXCollections.observableArrayList("Oriented Bounding Box", "Straight Skeleton", "Offset", "OBB then SS"));
        dialog.setTooltip(new Tooltip("Select a Process with which you will divide every selected parcels"));
        ChangeListener<String> changeListener = (observable, oldValue, newValue) -> {
            if (newValue != null)
                setProcess(dialog.getSelectionModel().getSelectedItem());
        };

        dialog.getSelectionModel().selectedItemProperty().addListener(changeListener);
        VBox box = new VBox();
        box.getChildren().addAll(new Label("Choose process"), dialog);
        return box;
    }

    public static void setProcess(String s) {
        switch (s) {
            // case "Median Skeleton":
            // Tools.notImplementedYet();
            case "Straight Skeleton":
                Workflow.PROCESS = DivisionType.SS;
                break;
            case "Offset":
                Workflow.PROCESS = DivisionType.SSoffset;
                break;
            case "Oriented Bounding Box":
                Workflow.PROCESS = DivisionType.OBB;
                break;
                case "OBB then SS":
                Workflow.PROCESS = DivisionType.OBBThenSS;
                break;
        }
    }

    public static CheckBox canBeIsolatedParcel() {
        CheckBox checkBox = new CheckBox("Parcel can be isolate from the road");
        checkBox.setOnAction(action -> DensificationWorkflow.ISOLATEDPARCEL = checkBox.isSelected());
        return checkBox;
    }

    public static CheckBox doWeKeepExistingRoads() {
        CheckBox checkBox = new CheckBox("Keep existing roads");
        checkBox.setOnAction(action -> ZoneDivisionWorkflow.KEEPEXISTINGROADS = checkBox.isSelected());
        return checkBox;
    }

    public static void doGeneratePeripheralRoad() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Do we generate peripheral roads ?");
        alert.setHeaderText("Do you want to generate peripheral roads around each zones ?");
        GridPane expContent = new GridPane();
        Label label = new Label("If the zone are not near from a road, the straight skeleton division (or offset) process won't produce anything but a single parcel");
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);

        Button buttonMarkAll = new Button("Generate Peripheral road");
        buttonMarkAll.setOnAction(e -> {
            StraightSkeletonDivision.setGeneratePeripheralRoad(true);
            alert.close();
        });

        ButtonType buttonTypeCancel = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);
        expContent.add(buttonMarkAll, 0, 1);
        alert.getButtonTypes().setAll(buttonTypeCancel);
        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setContent(expContent);
        alert.showAndWait();
    }
}
