package fr.ign.artiscales.pm.gui.tools;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import org.geotools.data.simple.SimpleFeatureCollection;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Tools {

    // public static void main(String[] args) {
    // // DataStore ds = Collec.getDataStore(new File("../ParcelManager/src/main/resources/GeneralTest/road.gpkg"));
    // // System.out.println(getCollectionName(ds.getFeatureSource(ds.getTypeNames()[0]).getFeatures()));
    // // ds.dispose();
    // System.out.println(saveToNewFile(new File("/tmp"), "ConsolidationDivisionOutput.gpkg"));
    // }

    public static void printEx(Exception ex) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Exception Dialog");
        alert.setHeaderText("Problem");
        alert.setContentText("See java output here");

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public static void notImplementedYet() {
        // Hyperlink sourceCode = new Hyperlink("maxime.colomb@protonmail.com");
        // sourceCode.setOnAction(e -> {
        // try {
        // new ProcessBuilder("xdg-open mailto://maxime.colomb@protonmail.com").start();
        // } catch (IOException ee) {
        // ee.printStackTrace();
        // }
        // });
        // Alert alert = new Alert(AlertType.CONFIRMATION, "Delete " + selection + " ?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setContentText("In a hurry ? Fast the process - Contact me at maxime.colomb@protonmail.com");
        alert.setHeaderText("Not Implemented Yet");
        alert.showAndWait();
    }

    public static String getCollectionName(SimpleFeatureCollection sfc) {
        try {
            return sfc.getSchema().getName().toString();
        } catch (NullPointerException np) {
            return "";
        }
    }

    public static File getNewFileToSave(File folder, String name) {
        File supposedName = new File(folder, name);
        if (!supposedName.exists())
            return supposedName;
        int i = 1;
        while (new File(folder, makeNewName(name, i)).exists())
            i++;
        return new File(folder, makeNewName(name, i));
    }

    private static String makeNewName(String name, int i) {
        String attr = "." + name.split("\\.")[name.split("\\.").length - 1];
        return name.replace(attr, "") + "-" + i + attr;
    }

    public static void openWebpage(String url) {
        try {
            new ProcessBuilder("x-www-browser", url).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getFinishAlert(File outFolder, String processName) {
        getProcessAlert(processName + " has successfully run. Find the result in the " + outFolder.getAbsolutePath() + " folder", "Success");
    }

    public static void getStartAlert(String processName) {
        getProcessAlert(processName + " is currently processing. Please wait for the new popup", "Done");
    }

    public static void getProcessAlert(String txt, String headerStatus) {
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
        alert2.setContentText(txt);
        alert2.setHeaderText(headerStatus);
        alert2.show();
    }
}
