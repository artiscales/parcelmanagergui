package fr.ign.artiscales.pm.gui;

import fr.ign.artiscales.pm.gui.setParameters.ParcelImport;
import fr.ign.artiscales.pm.gui.setParameters.ProfileUrbanFabricImport;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.tools.parameter.ProfileUrbanFabric;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.geotools.data.simple.SimpleFeatureCollection;

import java.io.File;
import java.util.List;

public abstract class GeoDataImport {
    public SimpleFeatureCollection sfcParcel;
    public File parcelFile, roadFile, buildingFile, zoneFile, outFolder, lastFolder, rootFolder, scenarioFile;
    public ProfileUrbanFabric profile;
    public String roadName = "", parcelName = "", buildingName = "", zoneName = "";
    public String workflowName;
    public List<String> necessariesElements, optionalElements;
    public Pane rootGroup;

    public static void alertIfMissing(String missing) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setContentText("Problem with the " + missing + " File. It is either not set or doesn't exists");
        alert.setHeaderText("Information missing");
        alert.showAndWait();
    }

    public Scene getScene() {
        return new Scene(rootGroup, App.width, App.height);
    }

    public Button getOutFolderButton(Stage stage) {
        DirectoryChooser directoryChooserOut = new DirectoryChooser();
        // directoryChooserOut.setInitialDirectory(lastFolder);
        directoryChooserOut.setInitialDirectory(new File("/tmp/"));
        Button buttonParcel = new Button("Select an output folder");
        buttonParcel.setOnAction(e -> {
            outFolder = directoryChooserOut.showDialog(stage);
            // lastFolder = outFolder;
        });
        return buttonParcel;
    }


    public Button getButtonParcel(Stage stage) {
        return getButtonParcel(stage, true);
    }

    /**
     * Create a button to import parcel geofiles and load them into the program. Additional work can be done, i.e to check if parcels are properly marked.
     *
     * @param stage
     * @param checkMark if true, the program will check if at least one of the parcel is marked and if not, will ask to mark every parcel
     * @return a {@link Button} importing parcel geofiles and load them
     */
    public Button getButtonParcel(Stage stage, boolean checkMark) {
        FileChooser fileChooserParcel = new FileChooser();
        fileChooserParcel.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Geopackage", "*.gpkg"));
        Button buttonParcel = new Button("Select a parcel File");
        buttonParcel.setOnAction(e -> {
            fileChooserParcel.setInitialDirectory(getLastFolder());
//            f = new FileInputStream(fileChooserParcel.showOpenDialog(stage));
            parcelFile = fileChooserParcel.showOpenDialog(stage);
            if (parcelFile != null) {
                sfcParcel = ParcelImport.parcelImport(parcelFile, checkMark);
                parcelName = Tools.getCollectionName(sfcParcel);
                setLastFolder(parcelFile.getParentFile());
                // setVBox(vb, buttonParcel);
                // stage.setWidth(stage.getWidth() + 0.0001);
            }
        });
        return buttonParcel;
    }

    public Button getButtonProfileUrbanFabric(Stage stage) {
        // parcel import
        FileChooser fileChooserPUF = new FileChooser();
        fileChooserPUF.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
        Button buttonPUF = new Button("Select an Urban Fabric Profile");
        buttonPUF.setOnAction(e -> {
            fileChooserPUF.setInitialDirectory(getLastFolder());
            File f = fileChooserPUF.showOpenDialog(stage);
            if (f != null) {
                profile = ProfileUrbanFabricImport.getProfileUrbanFabricImport(f);
                setLastFolder(f.getParentFile());
            }
        });
        return buttonPUF;
    }

    public File getLastFolder() {
        return lastFolder;
    }

    public void setLastFolder(File f) {
        lastFolder = f;
    }

    public Button getButtonRoad(Stage stage) {
        // road import
        FileChooser fileChooserRoad = new FileChooser();
        fileChooserRoad.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Geopackage", "*.gpkg"));
        Button buttonRoad = new Button("Select a road layer");
        buttonRoad.setOnAction(e -> {
            fileChooserRoad.setInitialDirectory(getLastFolder());
            File f = fileChooserRoad.showOpenDialog(stage);
            if (f != null) {
                roadFile = f;
                roadName = roadFile.getName();
                setLastFolder(f.getParentFile());
            }
        });
        return buttonRoad;
    }

    public Button getButtonBuilding(Stage stage) {
        FileChooser fileChooserBuilding = new FileChooser();
        fileChooserBuilding.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Geopackage", "*.gpkg"));
        Button buttonBuilding = new Button("Select a building layer");
        buttonBuilding.setOnAction(e -> {
            fileChooserBuilding.setInitialDirectory(getLastFolder());
            File f = fileChooserBuilding.showOpenDialog(stage);
            if (f != null) {
                buildingFile = f;
                buildingName = buildingFile.getName();
                setLastFolder(f.getParentFile());
            }
        });
        return buttonBuilding;
    }

    public boolean checkIfFilled() {
        boolean result = true;
        for (String s : necessariesElements) {
            switch (s) {
                case "outputFolder":
                    if (outFolder == null || outFolder.getAbsolutePath().equals("") || !outFolder.exists()) {
                        alertIfMissing(s);
                        result = false;
                    }
                    break;
                case "rootFolder":
                    if (rootFolder == null || rootFolder.getAbsolutePath().equals("") || !rootFolder.exists()) {
                        alertIfMissing(s);
                        result = false;
                    }
                    break;
                case "parcel":
                    if (sfcParcel == null || sfcParcel.isEmpty()) {
                        alertIfMissing(s);
                        result = false;
                    }
                    break;
                case "road":
                    if (roadFile == null || roadFile.getAbsolutePath().equals("") || !roadFile.exists()) {
                        alertIfMissing(s);
                        result = false;
                    }
                    break;
                case "building":
                    if (buildingFile == null || buildingFile.getAbsolutePath().equals("") || !buildingFile.exists()) {
                        alertIfMissing(s);
                        result = false;
                    }
                    break;
                case "profileUrbanFabric":
                    if (profile == null) {
                        alertIfMissing(s);
                        result = false;
                    }
                    break;
                case "zone":
                    if (zoneFile == null) {
                        alertIfMissing(s);
                        result = false;
                    }
                    break;
                case "scenarioFile":
                    if (scenarioFile == null) {
                        alertIfMissing(s);
                        result = false;
                    }
                    break;
            }
        }
        return result;
    }

    public void setParcelName(String n) {
        parcelName = n;
    }

}
