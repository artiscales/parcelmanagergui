package fr.ign.artiscales.pm.gui.process;

import fr.ign.artiscales.pm.division.StraightSkeletonDivision;
import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.tools.CreateUrbanProfile;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;

public class StraightSkeletonProcess extends ProcessScene {

    public StraightSkeletonProcess(Stage stage) {
        super(stage);
        stage.setTitle("Straight skeleton process");
        necessariesElements = Arrays.asList("parcel", "road", "outputFolder");

        VBox parcel = new VBox();
        parcel.getChildren().addAll(getButtonParcel(stage), new Label(parcelName));

        VBox road = new VBox();
        road.getChildren().addAll(getButtonRoad(stage), new Label(roadName));

        VBox outputFolder = new VBox();
        outputFolder.getChildren().addAll(getOutFolderButton(stage), new Label(outFolder != null ? outFolder.getName() : ""));

        String process = "ss";
        CreateUrbanProfile cub = new CreateUrbanProfile(process);
        VBox params = cub.getUrbanProfileParams(process);
        cub.setNeededFields(process);

        CheckBox checkGeneratePeripheralRoad = new CheckBox("generate peripheral road");
        checkGeneratePeripheralRoad.setOnAction(action -> StraightSkeletonDivision.setGeneratePeripheralRoad(checkGeneratePeripheralRoad.isSelected()));

        CheckBox checkDebug = new CheckBox("Enable DEBUG mode");
        checkDebug.setOnAction(action -> StraightSkeletonDivision.setDEBUG(checkDebug.isSelected()));

        Button importPUF = cub.buttonImport(stage);

        Button runButton = new Button("Run");
        runButton.setOnAction(e -> {
            if (cub.getConfirmationAction() && checkIfFilled()) {
                this.profile = cub.getProfile();
                try {
                    CollecMgmt.exportSFC(StraightSkeletonDivision.runTopologicalStraightSkeletonParcelDecomposition(sfcParcel, roadFile,
                            "NOM_VOIE_G", "IMPORTANCE", profile.getMaxDepth(), profile.getMaxDistanceForNearestRoad(),
                            profile.getMinimalArea(), profile.getMinimalWidthContactRoad(), profile.getMaxWidth(), 0.1, profile.getLaneWidth(), Tools.getNewFileToSave(outFolder, "StraightSkeletonDivided" + App.geoFormat).getName()), Tools.getNewFileToSave(outFolder, "StraightSkeletonDivided" + App.geoFormat));
                    Tools.getProcessAlert("Straight skeleton process has successfully been proceeded. Find the result in the " + outFolder.getAbsolutePath() + " folder", "Done");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
        rootGroup.getChildren().addAll(new Label("Geographic data to import"), parcel, road, outputFolder, params, importPUF, checkDebug, checkGeneratePeripheralRoad, runButton);
    }
}
