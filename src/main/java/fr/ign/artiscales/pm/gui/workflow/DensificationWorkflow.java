package fr.ign.artiscales.pm.gui.workflow;

import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.pm.workflow.Densification;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import fr.ign.artiscales.tools.geometryGeneration.CityGeneration;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DensificationWorkflow extends WorkflowScene {

    static List<String> necessariesElements = Arrays.asList("parcel", "building" , "profileUrbanFabric", "outputFolder");
    static List<String> optionalElements = Collections.singletonList("road");
    public static boolean ISOLATEDPARCEL;

    public DensificationWorkflow(Stage stage) {
        super(stage,"Densification", necessariesElements, optionalElements);
        stage.setTitle("Densification Workflow");
        getParameters(stage);
    }

    @Override
    public void run() {
        if (checkIfFilled())
            try {
                // Task<Void> longRunningTask = new Task<Void>() {
                //
                // @Override
                // protected Void call() throws Exception {

//                Tools.getStartAlert("Workflow <b>Densification</b>");
                CollecMgmt.exportSFC((new Densification()).densification(sfcParcel, CityGeneration.createUrbanBlock(sfcParcel), outFolder, buildingFile, roadFile, profile, ISOLATEDPARCEL),
                        Tools.getNewFileToSave(outFolder, "DensificationOutput" + App.geoFormat));
//                Tools.getFinishAlert(outFolder, "Workflow <b>Densification</b>");

                // Platform.runLater(() -> status.setText("Connected"));
                // return null;
                // }
                // };
                // button.setOnAction(e -> {
                // new Thread(longRunningTask).start();
                // });
            } catch (Exception e) {
                Tools.printEx(e);
                e.printStackTrace();
            }
    }

}
