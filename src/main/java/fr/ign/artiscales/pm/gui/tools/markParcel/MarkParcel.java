package fr.ign.artiscales.pm.gui.tools.markParcel;

import fr.ign.artiscales.pm.gui.GeoDataImport;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;

public abstract class MarkParcel extends GeoDataImport {

    CheckBox reuseMarking = new CheckBox("Only mark already marked parcels");

    public Button getRunButton() {
        Button b = new Button("Run");
        b.setOnAction(e -> run());
        return b;
    }

    public abstract void run();
}
