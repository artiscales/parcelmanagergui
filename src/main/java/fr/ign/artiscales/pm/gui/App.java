package fr.ign.artiscales.pm.gui;

import fr.ign.artiscales.pm.gui.process.ProcessScene;
import fr.ign.artiscales.pm.gui.scenario.PMScenarioScene;
import fr.ign.artiscales.pm.gui.tools.CreateUrbanProfile;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.pm.gui.tools.markParcel.MarkParcelScene;
import fr.ign.artiscales.pm.gui.useCase.UseCaseScene;
import fr.ign.artiscales.pm.gui.workflow.WorkflowScene;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.FileNotFoundException;

/**
 * JavaFX App // tuto with lot of itmes here : http://tutorials.jenkov.com/javafx/button.html // and to make javafx work follow that
 * https://stackoverflow.com/questions/52144931/how-to-add-javafx-runtime-to-eclipse-in-java-11
 */
public class App extends Application {

    public static double width = 1000;
    public static double height = 800;
    public static Scene mainScene;
    public static Stage primaryStageS;
    public static String geoFormat = ".gpkg";
    public static String versionNumber = "v1.3";

    public static void main(String[] args) {
        Application.launch(args);
    }

    public static Hyperlink returnToMain(Stage stage) {
        Hyperlink link = new Hyperlink("return to main page");
        link.setOnAction(e -> {
            stage.setScene(mainScene);
            stage.setTitle("Parcel Manager Main Page");
        });
        return link;
    }

    public static GridPane getFooter() {
        Hyperlink creditFooter = new Hyperlink("Credit");
        creditFooter.setOnAction(event -> primaryStageS.setScene(getCreditScene()));

        Hyperlink helpFooter = new Hyperlink("Help");
        helpFooter.setOnAction(event -> primaryStageS.setScene(getHelpScene()));

        Hyperlink markParcelTools = new Hyperlink("More: Mark Parcels");
        markParcelTools.setOnAction(event -> primaryStageS.setScene((new MarkParcelScene()).getScene(primaryStageS)));

        Hyperlink CreatePUFTools = new Hyperlink("More: Create Urban Profile");
        CreatePUFTools.setOnAction(event -> primaryStageS.setScene(CreateUrbanProfile.getCreateUrbanProfileScene(primaryStageS)));

        final GridPane footer = new GridPane();
        GridPane.setConstraints(creditFooter, 0, 0);
        GridPane.setConstraints(helpFooter, 1, 0);
        GridPane.setConstraints(markParcelTools, 2, 0);
        GridPane.setConstraints(CreatePUFTools, 3, 0);

        footer.setHgap(4);
        footer.setVgap(1);
        footer.getChildren().addAll(creditFooter, helpFooter, markParcelTools, CreatePUFTools);
        return footer;
    }

    public static Scene getHelpScene() {
        primaryStageS.setTitle("Help");
        Label l = new Label("Parcel Manager is a library providing functions, algorithms and workflows to process various parcel reshaping operations.\n" +
                "There are multiple ways of using this library.\n\n" +
                "Divide a single or few parcels: choose one of the multiple implemented processes.\n" +
                "Only marked parcels will be divided\n\n" +
                "Process various operations on a zone: use a pre-designed workflow that runs various processes such as parcel division," +
                " parcel aggregation,\nor other (attribute attribution, sizes thresholds, etc.). Workflow description can be found in Colomb et al 2021\n\n" +
                "On a community or on multiple communities: create advanced scenarios using simple .json files (see documentation link bellow).\n\n" +
                "To run predefined workflow experiments, such as densification studies or parcel evolution studies, run existing use cases;\n\n\n" +

                "Attributes are now only adapted for a french context but implementation of new country's nomenclature are possible.\n" +
                "Please refer to the documentation link bellow.\n\n\n");
        Hyperlink attributeHelp = new Hyperlink("Parcel attributes and marking");
        attributeHelp.setOnAction(e -> Tools.openWebpage("https://framagit.org/artiscales/parcelmanager/-/blob/master/src/main/resources/doc/AttributePolicy.md"));
        Hyperlink scenarioHelp = new Hyperlink("Scenario creation");
        scenarioHelp.setOnAction(e -> Tools.openWebpage("https://framagit.org/artiscales/parcelmanager/-/blob/master/src/main/resources/doc/scenarioCreation.md"));
        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(10, 20, 10, 20));
        VBox vb = new VBox();
        vb.getChildren().addAll(l, attributeHelp, scenarioHelp);
        pane.setCenter(vb);
        pane.setBottom(returnToMain(primaryStageS));
        return new Scene(pane, width, height);
    }

    public static Scene getCreditScene() {
        primaryStageS.setTitle("Credits");
        Label l = new Label("Parcel Manager "+versionNumber+". \n\nThis is a simple graphical user interface for the Parcel Manager model developed in a post-doc contract for the University of Franche-Comté. \n" +
                "This GUI has been designed to go with the scientific article : Colomb et al. 2021 (to be published).\n" +
                "More complete functionalities are available in the raw java model.\n" +
                "As developments isn't finished yet, more functionalities will be added from time to time.\n" +
                "Everything is under the AGPLV3 licence and reusable under certain conditions.\n\n");
        Hyperlink licence = new Hyperlink("Licence Affero GPL 3");
        licence.setOnAction(e -> Tools.openWebpage("https://www.gnu.org/licenses/agpl-3.0.html"));
        Hyperlink sourceCode = new Hyperlink("Source code Parcel Manager");
        sourceCode.setOnAction(e -> Tools.openWebpage("https://framagit.org/artiscales/parcelmanager"));
        Hyperlink sourceCodeGUI = new Hyperlink("Source code Parcel Manager GUI");
        sourceCodeGUI.setOnAction(e -> Tools.openWebpage("https://framagit.org/artiscales/parcelmanagergui"));
        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(10, 20, 10, 20));

        VBox vb = new VBox();
        vb.getChildren().addAll(l, licence, sourceCode, sourceCodeGUI);
        pane.setCenter(vb);
        pane.setBottom(returnToMain(primaryStageS));
        return new Scene(pane, width, height);
    }

    @Override
    public void start(Stage primaryStage) throws FileNotFoundException {
        primaryStageS = primaryStage;
        primaryStageS.setTitle("Parcel Manager "+versionNumber+" - Main Page");
        // primaryStage.setFullScreen(true);
        primaryStage.initStyle(StageStyle.DECORATED);
        // primaryStage.initModality(Modality.WINDOW_MODAL);
        BorderPane bp = new BorderPane();
        bp.setPadding(new Insets(10, 20, 10, 20));
        GridPane title = new GridPane();
        title.setHgap(24);
        title.setVgap(24);
        Text scenetitle = new Text("Welcome to Parcel Manager "+versionNumber);
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        title.add(scenetitle, 0, 1);
        title.add(new Label("Choose what you want to work with"), 0, 2);
        bp.setTop(title);
        title.setAlignment(Pos.CENTER);

        Button buttonProcess = new Button("Process on a single input");
        buttonProcess.setText("Processes");
        buttonProcess.setOnAction(event -> primaryStage.setScene((new ProcessScene.MainProcess(primaryStage)).getScene()));
        Button buttonGoal = new Button("Workflows");
        buttonGoal.setOnAction(event -> primaryStage.setScene((new WorkflowScene.MainWorkflow(primaryStage)).getScene()));
        Button buttonScenario = new Button("Scenarios");
        buttonScenario.setOnAction(event ->  primaryStage.setScene((new PMScenarioScene.MainScenario(primaryStage)).getScene()));
        Button buttonUseCase = new Button("Use Cases");
        buttonUseCase.setOnAction(event -> primaryStage.setScene((new UseCaseScene.MainUseCase(primaryStage).getScene())));
        // Dialog format = setFormat();
        GridPane footer = getFooter();
        bp.setBottom(footer);

//        Image image = new Image("./fig.png");
//        ImageView imageView = new ImageView(image);
//        imageView.setX(10);
//        imageView.setY(10);
//        imageView.setFitWidth(575);
//        imageView.setPreserveRatio(true);

        VBox body = new VBox();
        body.getChildren().addAll(new VBox(new Label("A single (or few) parcel:"), buttonProcess), new VBox(new Label("A zone:"), buttonGoal), new VBox(new Label("A community or a region:"), buttonScenario), new VBox(new Label("Already designed experience plans:"), buttonUseCase));
        body.setPadding(new Insets(50, 20, 50, 20));
        body.setSpacing(24);
        bp.setCenter(body);

        mainScene = new Scene(bp, width, height);
        // scene.setCursor(Cursor.CROSSHAIR);
        primaryStage.setScene(mainScene);

        // primaryStage.scene
        primaryStage.show();
    }


}
