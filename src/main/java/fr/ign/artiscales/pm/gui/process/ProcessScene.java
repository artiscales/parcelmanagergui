package fr.ign.artiscales.pm.gui.process;

import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.GeoDataImport;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public abstract class ProcessScene extends GeoDataImport {

    public ProcessScene(Stage stage) {

        stage.setTitle("Select Process");
        Button buttonOBB = new Button("Oriented Bounding Box");
        buttonOBB.setOnAction(e -> {
            OrientedBoundingBoxProcess obb = new OrientedBoundingBoxProcess(stage);
            stage.setScene(obb.getScene());
        });
        Button buttonFS = new Button("Flag Division");
        buttonFS.setOnAction(e -> {
            FlagDivisionProcess fcp = new FlagDivisionProcess(stage);
            stage.setScene(fcp.getScene());
        });
        Button buttonSS = new Button("Straight Skeleton");
        buttonSS.setOnAction(e -> {
            StraightSkeletonProcess ss = new StraightSkeletonProcess(stage);
            stage.setScene(ss.getScene());
        });
        Button buttonOBBSS = new Button("OBB then SS");
        buttonOBBSS.setOnAction(e -> {
            OBBThenSSProcess obbss = new OBBThenSSProcess(stage);
            stage.setScene(obbss.getScene());
        });

        final GridPane inputGridPane = new GridPane();
        GridPane.setConstraints(buttonSS, 0, 0);
        GridPane.setConstraints(buttonFS, 1, 0);
        GridPane.setConstraints(buttonOBB, 2, 0);
        GridPane.setConstraints(buttonOBBSS, 3, 0);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
        inputGridPane.getChildren().addAll(buttonSS, buttonFS, buttonOBB, buttonOBBSS);
        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
        rootGroup.getChildren().add(App.returnToMain(stage));
    }

    public static class MainProcess extends ProcessScene {
        public MainProcess(Stage stageGoal) {
            super(stageGoal);
        }
    }
}
