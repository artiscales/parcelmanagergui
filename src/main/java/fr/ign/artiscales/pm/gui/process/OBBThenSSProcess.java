package fr.ign.artiscales.pm.gui.process;

import fr.ign.artiscales.pm.division.OBBThenSS;
import fr.ign.artiscales.pm.gui.tools.CreateUrbanProfile;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;

public class OBBThenSSProcess extends ProcessScene {

    public OBBThenSSProcess(Stage stage) {
        super(stage);
        stage.setTitle("Oriented Bounding Box then Straight Skeleton process");
        necessariesElements = Arrays.asList("parcel", "road", "outputFolder");
        VBox parcel = new VBox();
        parcel.getChildren().addAll(getButtonParcel(stage), new Label(parcelName));

        VBox road = new VBox();
        road.getChildren().addAll(getButtonRoad(stage), new Label(roadName));

        VBox outputFolder = new VBox();
        outputFolder.getChildren().addAll(getOutFolderButton(stage), new Label(outFolder != null ? outFolder.getName() : ""));

        String process = "OBBThenSS";
        CreateUrbanProfile cub = new CreateUrbanProfile(process);
        VBox params = cub.getUrbanProfileParams(process);
        cub.setNeededFields(process);

        Button importPUF = cub.buttonImport(stage);
        Button runButton = new Button("Run");
        runButton.setOnAction(e -> {
            if (cub.getConfirmationAction() && checkIfFilled()) {
                this.profile = cub.getProfile();
                try {
                    CollecMgmt.exportSFC(OBBThenSS.applyOBBThenSS(sfcParcel, roadFile, profile), Tools.getNewFileToSave(outFolder, "OBBThenSSDivided"));
                    Tools.getProcessAlert(process + " process has successfully been proceeded. Find the result in the " + outFolder.getAbsolutePath() + " folder", "Done");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
        rootGroup.getChildren().addAll(new Label("Geographic data to import"), parcel, road, outputFolder, params, importPUF, runButton);
    }
}
