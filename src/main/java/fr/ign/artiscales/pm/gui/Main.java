//package fr.ign.artiscales.pm.gui;
//
//import javafx.application.Application;
//import javafx.geometry.Insets;
//import javafx.scene.Scene;
//import javafx.scene.control.Button;
//import javafx.scene.control.Label;
//import javafx.scene.layout.GridPane;
//import javafx.scene.layout.Pane;
//import javafx.scene.layout.VBox;
//import javafx.stage.FileChooser;
//import javafx.stage.Stage;
//import javafx.stage.StageStyle;
//
//import java.io.File;
//
//public class Main extends Application {
//	public static Stage primaryStageS;
//	public static Scene mainScene;
//
//	public void start(Stage primaryStage) {
//		primaryStageS = primaryStage;
//		primaryStage.setTitle("Parcel Manager Main Page");
//		primaryStage.initStyle(StageStyle.DECORATED);
//		VBox main = new VBox(new Label("Test program"));
//		mainScene = new Scene(main, 800, 600);
//		primaryStage.setScene((new Test(primaryStage)).getScene());
//		primaryStage.show();
//	}
//
//	public static void main(String[] args) {
//		Application.launch(args);
//	}
//
//	public static class Object1 {
//		String name;
//
//		public Object1(File f) {
//			name = f.getName();
//		}
//
//		public String getName() {
//			return name;
//		}
//	}
//
//	public static class Test {
//		Object1 collec;
//		String collecName;
//		File lastFolder;
//		Pane rootGroup;
//
//		public Test(Stage stage) {
//			setButtons(stage);
//		}
//
//		public void setButtons(Stage stageGoal) {
//			VBox vbox = new VBox();
//			Button b = getButton(stageGoal);
//			vbox.getChildren().addAll(b, new Label(getCollecName() == null ? "no name" : collecName));
//			final GridPane inputGridPane = new GridPane();
//			GridPane.setConstraints(vbox, 0, 0);
//			inputGridPane.getChildren().addAll(vbox);
//			rootGroup = new VBox(12);
//			rootGroup.getChildren().addAll(inputGridPane);
//			rootGroup.setPadding(new Insets(12, 12, 12, 12));
//		}
//
//		public Button getButton(Stage stage) {
//			FileChooser fileChooserParcel = new FileChooser();
//			Button button = new Button("Select a File");
//			button.setOnAction(e -> {
//				fileChooserParcel.setInitialDirectory(getLastFolder());
//
//				File f = fileChooserParcel.showOpenDialog(stage);
//				if (f != null) {
//					collec = new Object1(f);
//					setLastFolder(f.getParentFile());
//					setCollecName(collec);
//					setButtons(stage); // tried to reload every buttons - doesn't work
//					stage.setWidth(stage.getWidth() + 0.0001); // found this dirty hack but doesn't work
//				}
//			});
//			return button;
//		}
//
//		public void setCollecName(Object1 o1) {
//			collecName = o1.getName();
//		}
//
//		public String getCollecName() {
//			return collecName;
//		}
//
//		public File getLastFolder() {
//			return lastFolder;
//		}
//
//		public void setLastFolder(File folder) {
//			System.out.println("set last folder: " + folder);
//			lastFolder = folder;
//		}
//
//		private Scene getScene() {
//			return new Scene(rootGroup, 800, 600);
//		}
//	}
//}