package fr.ign.artiscales.pm.gui.workflow;

import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.GeoDataImport;
import fr.ign.artiscales.pm.gui.setParameters.GeneralOptions;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.pm.workflow.Workflow;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import java.io.File;
import java.util.List;

public abstract class WorkflowScene extends GeoDataImport {

    String title = "Workflows";

    public WorkflowScene(Stage stageGoal, String nameWorkflow, List<String> necessariesElements, List<String> optionalElements) {
        // main = new VBox(new Label("Run a workflow"));
        workflowName = nameWorkflow;
        this.necessariesElements = necessariesElements;
        this.optionalElements = optionalElements;
        stageGoal.setTitle(title);
        Button buttonConsolidationDivision = new Button("Zone Consolidation / Division");
        buttonConsolidationDivision.setOnAction(e -> {
            ConsolidationWorkflowScene cdg = new ConsolidationWorkflowScene(stageGoal);
            stageGoal.setScene(cdg.getScene());
        });
        Button buttonDensification = new Button("Densification");
        buttonDensification.setOnAction(e -> {
            DensificationWorkflow d = new DensificationWorkflow(stageGoal);
            stageGoal.setScene(d.getScene());
        });
        Button buttonZoneDivision = new Button("Zone Division");
        buttonZoneDivision.setOnAction(e -> {
            ZoneDivisionWorkflow zd = new ZoneDivisionWorkflow(stageGoal);
            stageGoal.setScene(zd.getScene());
        });

        final GridPane inputGridPane = new GridPane();
        GridPane.setConstraints(buttonZoneDivision, 0, 0);
        GridPane.setConstraints(buttonDensification, 0, 1);
        GridPane.setConstraints(buttonConsolidationDivision, 0, 2);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
        inputGridPane.getChildren().addAll(buttonZoneDivision, buttonDensification, buttonConsolidationDivision);
        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
    }

    public static CheckBox isDebugMode() {
        CheckBox checkBox = new CheckBox("Enable DEBUG mode");
        checkBox.setOnAction(action -> Workflow.setDEBUG(checkBox.isSelected()));
        return checkBox;
    }

    public void getParameters(Stage stageGoal) {

        VBox parcel = new VBox();
        Button bP = getButtonParcel(stageGoal, !workflowName.equals("ZoneDivision")); //no parcel check for zone division because parcel doesn't get divided
        parcel.getChildren().addAll(bP, new Label(parcelName));

        VBox road = new VBox();
        Button bR = getButtonRoad(stageGoal);
        road.getChildren().addAll(bR, new Label(roadName));
        if (optionalElements != null && optionalElements.contains("road"))
            road.getChildren().addAll(new Label("optional for OBB process"));

        VBox puf = new VBox();
        Button bPUF = getButtonProfileUrbanFabric(stageGoal);
        puf.getChildren().addAll(bPUF, new Label(profile != null ? profile.getNameBuildingType() : ""));

        VBox of = new VBox();
        Button bOF = getOutFolderButton(stageGoal);
        of.getChildren().addAll(bOF, new Label(outFolder != null ? outFolder.getName() : ""));

        CheckBox checkDebug = WorkflowScene.isDebugMode();

        VBox setProcess = new VBox();
        VBox setIsolatedParcel = new VBox();
        VBox building = new VBox();
        if (workflowName.equals("Densification")) {
            setIsolatedParcel.getChildren().addAll(GeneralOptions.canBeIsolatedParcel());
            building.getChildren().addAll(getButtonBuilding(stageGoal), new Label(buildingName));
        } else
            setProcess = GeneralOptions.getSetProcess();

        VBox zone = new VBox();
        VBox doWeKeepExistingRoad = new VBox();
        if (workflowName.equals("ZoneDivision")) {
            doWeKeepExistingRoad.getChildren().addAll(GeneralOptions.doWeKeepExistingRoads(), new Label(zoneName));
            zone.getChildren().addAll(getButtonZone(stageGoal), new Label(zoneName));
        }
        Button buttonLaunch = new Button("Launch Workflow");
        buttonLaunch.setOnAction(e -> {
            if (checkIfFilled()) {
                run();
                Tools.getFinishAlert(outFolder, "Workflow " + workflowName);
            }
        });

        final GridPane inputGridPane = new GridPane();
        inputGridPane.setHgap(10);
        inputGridPane.setVgap(10);
        GridPane.setConstraints(parcel, 0, 0);
        GridPane.setConstraints(road, 1, 0);
        GridPane.setConstraints(building, 2, 0);
        GridPane.setConstraints(zone, 3, 0);
        GridPane.setConstraints(puf, 4, 0);
        GridPane.setConstraints(of, 5, 0);
        GridPane.setConstraints(checkDebug, 0, 1);
        GridPane.setConstraints(setIsolatedParcel, 1, 1);
        GridPane.setConstraints(doWeKeepExistingRoad, 2, 1);
        GridPane.setConstraints(setProcess, 3, 1);
        GridPane.setConstraints(buttonLaunch, 0, 3);
        inputGridPane.setHgap(5);
        inputGridPane.setVgap(5);
        inputGridPane.getChildren().addAll(parcel, road, building, zone, puf, of, checkDebug, setIsolatedParcel, doWeKeepExistingRoad, setProcess, buttonLaunch);

        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
        rootGroup.getChildren().add(App.returnToMain(stageGoal));
    }

    public abstract void run();

    public Button getButtonZone(Stage stage) {
        // zone import
        FileChooser fileChooserZone = new FileChooser();
        fileChooserZone.getExtensionFilters().addAll(new ExtensionFilter("Geopackage", "*.gpkg"));
        Button buttonRoad = new Button("Select a zone layer");
        buttonRoad.setOnAction(e -> {
            fileChooserZone.setInitialDirectory(getLastFolder());
            File f = fileChooserZone.showOpenDialog(stage);
            if (f != null) {
                zoneFile = f;
                zoneName = zoneFile.getName();
                setLastFolder(f.getParentFile());
            }
        });
        return buttonRoad;
    }

    public static class MainWorkflow extends WorkflowScene {

        public MainWorkflow(Stage stageGoal) {
            super(stageGoal,null, null, null);
        }

        @Override
        public void run() {
        }
    }
}

