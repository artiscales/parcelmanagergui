package fr.ign.artiscales.pm.gui.useCase;

import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.GeoDataImport;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.pm.usecase.UseCase;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.util.List;

public abstract class UseCaseScene extends GeoDataImport {

    public UseCaseScene(Stage stageGoal, List<String> necessariesElements) {
        this.necessariesElements = necessariesElements;

        stageGoal.setTitle("Use Cases");
        Button buttonTestScenario = new Button("Test Scenario");
        buttonTestScenario.setOnAction(e -> {
            TestScenario gt = new TestScenario(stageGoal);
            stageGoal.setScene(gt.getScene());
        });
//        Button buttonDensificationStudy = new Button("Densification Study");
//        buttonDensificationStudy.setOnAction(e -> {
//            DensificationStudy d = new DensificationStudy(stageGoal);
//            stageGoal.setScene(d.getScene());
//        });
        Button buttonCompare = new Button("Compare simulated parcels with evolution");
        buttonCompare.setOnAction(e -> {
            ParcelComparison pc = new ParcelComparison(stageGoal);
            stageGoal.setScene(pc.getScene());
        });

        final GridPane inputGridPane = new GridPane();
        GridPane.setConstraints(buttonCompare, 0, 0);
//        GridPane.setConstraints(buttonDensificationStudy, 0, 1);
        GridPane.setConstraints(buttonTestScenario, 0, 2);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
//        inputGridPane.getChildren().addAll(buttonCompare, buttonDensificationStudy, buttonTestScenario);
        inputGridPane.getChildren().addAll(buttonCompare, buttonTestScenario);
        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
    }

    public static CheckBox isDebugMode() {
        CheckBox checkBox = new CheckBox("Enable DEBUG mode");
        checkBox.setOnAction(action -> UseCase.setDEBUG(checkBox.isSelected()));
        return checkBox;
    }

    public void getParameters(Stage stage) {
        VBox rootF = new VBox();
        DirectoryChooser directoryChooserRoot = new DirectoryChooser();
        Button buttonRoot = new Button("Select root folder");
        buttonRoot.setOnAction(e -> {
            rootFolder = directoryChooserRoot.showDialog(stage);
            // lastFolder = outFolder;
        });
        rootF.getChildren().addAll(new Label("Select the root folder of the use case (usually {Parcel Manager Project}/src/main/resources/{NameOfUseCase}/).\nNo progress bar are programmed. Understand that running use cases might take a lot of time."), buttonRoot);

        DirectoryChooser directoryChooserOut = new DirectoryChooser();
        Button buttonParcel = new Button("Select an output folder");
        buttonParcel.setOnAction(e -> {
            outFolder = directoryChooserOut.showDialog(stage);
            // lastFolder = outFolder;
        });

        Button buttonLaunch = new Button("Launch UseCase");
        buttonLaunch.setOnAction(e -> {
//            Tools.getStartAlert("UseCase <b>Test Scenario</b>");
            run();
            Tools.getFinishAlert(outFolder, "UseCase");
        });

        final GridPane inputGridPane = new GridPane();
        GridPane.setConstraints(rootF, 0, 0);
        GridPane.setConstraints(buttonParcel, 0, 2);
        GridPane.setConstraints(buttonLaunch, 0, 4);
        inputGridPane.setHgap(4);
        inputGridPane.setVgap(4);
        inputGridPane.getChildren().addAll(rootF, buttonParcel, buttonLaunch);

        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
        rootGroup.getChildren().add(App.returnToMain(stage));
    }

    protected abstract void run();

    public static class MainUseCase extends UseCaseScene {

        public MainUseCase(Stage stageGoal) {
            super(stageGoal, null);
        }

        @Override
        public void run() {
        }
    }
}
