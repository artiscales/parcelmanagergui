package fr.ign.artiscales.pm.gui.tools;

import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.tools.parameter.ProfileUrbanFabric;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CreateUrbanProfile {
    String maximalArea, minimalArea, name, minimalWidthContactRoad, streetLane, blockShape, streetWidth, laneWidth, maxDepth,
            maxDistanceForNearestRoad, maxWidth, lenDriveway, harmonyCoeff, irregularityCoeff, approxNumberParcelPerBlock;
    String process;
    TextField tfMA, tfMiA, tfName, tfWidth, tfSL, tfSW, tfBS, tfLW, tfDW, tfMD, tfMDNR, tfMW, tfN, tfHC, tfANPPB;
    List<String> neededFields;
    ProfileUrbanFabric profile;
    File outFolder;
    String tfvMA = "Set maximal area";
    String tfvMiA = "Set minimal area";
    String tfvName = "Set urban fabric name";
    String tfvWidth = "Set minimal width of contact with road";
    String tfvSL = "Set street level";
    String tfvSW = "Set street width";
    String tfvBS = "Set lane level";
    String tfvLW = "Set lane width";
    String tfvDW = "Set width of driveways";
    String tfvMD = "Set maximal depth of parcels (if not equal to 0, offset division)";
    String tfvMDNR = "Set maximal distance from the nearest road";
    String tfvMW = "Set maximal width of parcels";
    String tfvN = "Set irregularity coefficient";
    String tfvHC = "Set harmony coefficient";
    String tfvANPPB = "Set approximate number of parcels per block";

    public CreateUrbanProfile(String processTargeted) {
        this.process = processTargeted;
    }

    public static void notGoodValue(String val) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Value not set");
        alert.setHeaderText("Wrong " + val + " value");
        alert.setContentText("Value " + val + " either not set of has an invalid type");
        alert.showAndWait();
    }

    public static Scene getCreateUrbanProfileScene(Stage stage) {
        stage.setTitle("Create Urban Profile");
        CreateUrbanProfile u = new CreateUrbanProfile("all");
        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(10, 20, 10, 20));
        VBox vb = new VBox();
        vb.getChildren().addAll(u.getContent(), u.getOkButton(), u.getExportFolder(stage));
        pane.setCenter(vb);
        pane.setBottom(App.returnToMain(stage));
        return new Scene(pane, 800, 800);
    }


    public ProfileUrbanFabric getProfile() {
        return profile;
    }

    public GridPane getContent() {
        setNeededFields(process);
        GridPane pane = new GridPane();
        pane.setMaxWidth(Double.MAX_VALUE);
        pane.add(getUrbanProfileParams(process), 0, 0);
        return pane;
    }

    public void setNeededFields(String process) {
        switch (process) {
            case "obb":
                neededFields = Arrays.asList("name", "maximalArea", "minimalArea", "minimalWidthContactRoad", "laneWidth", "streetWidth", "streetLane", "blockShape");
                break;
            case "ss":
                neededFields = Arrays.asList("name", "minimalArea", "maxDepth", "maxDistanceForNearestRoad", "minWidth", "maxWidth", "laneWidth");
                break;
            case "flagDivision":
                neededFields = Arrays.asList("name", "maximalArea", "minimalArea", "minimalWidthContactRoad", "lenDriveway");
                break;
            case "all":
                neededFields = Arrays.asList("name", "maximalArea", "minimalArea", "minimalWidthContactRoad", "laneWidth", "streetWidth", "streetLane", "blockShape", "lenDriveway", "maxDepth", "maxDistanceForNearestRoad", "maxWidth", "approxNumberParcelPerBlock");
                break;
            case "OBBThenSS":
                neededFields = Arrays.asList("name", "maximalArea", "minimalArea", "minimalWidthContactRoad", "laneWidth", "streetWidth", "streetLane", "blockShape", "maxDepth", "maxDistanceForNearestRoad", "maxWidth", "approxNumberParcelPerBlock");
                break;
        }
    }

    public VBox getUrbanProfileParams(String process) {
        tfMA = new TextField(tfvMA);
        tfMiA = new TextField(tfvMiA);
        tfName = new TextField(tfvName);
        tfWidth = new TextField(tfvWidth);
        tfSL = new TextField(tfvSL);
        tfSW = new TextField(tfvSW);
        tfBS = new TextField(tfvBS);
        tfLW = new TextField(tfvLW);
        tfDW = new TextField(tfvDW);
        tfMD = new TextField(tfvMD);
        tfMDNR = new TextField(tfvMDNR);
        tfMW = new TextField(tfvMW);
        tfHC = new TextField(tfvHC);
        tfANPPB = new TextField(tfvANPPB);
        tfN = new TextField(tfvN);
        VBox v = new VBox();
        switch (process) {
            case "obb":
                v.getChildren().addAll(tfName, tfMA, tfMiA, tfWidth, tfSL, tfSW, tfBS, tfLW, tfHC, tfN);
                break;
            case "ss":
                tfvLW = "Set peripheral road width";
                tfLW = new TextField(tfvLW);
                tfvWidth = "Set minimal width of parcels";
                tfWidth = new TextField(tfvWidth);
                v.getChildren().addAll(tfName, tfMiA, tfMD, tfMDNR, tfMW, tfWidth, tfLW, tfN);
                break;
            case "densification":
                v.getChildren().addAll(tfName, tfMA, tfMiA, tfWidth, tfDW, tfHC, tfN);
                break;
            case "OBBThenSS":
                v.getChildren().addAll(tfName, tfMA, tfMiA, tfWidth, tfSL, tfSW, tfBS, tfLW, tfMD, tfMDNR, tfMW, tfANPPB, tfHC, tfN);
                break;
            case "all":
                v.getChildren().addAll(tfName, tfMA, tfMiA, tfWidth, tfSL, tfSW, tfBS, tfLW, tfDW, tfMD, tfMDNR, tfMW, tfANPPB, tfHC, tfN);
                break;
        }
        return v;
    }

    public Button getExportFolder(Stage stage) {
        DirectoryChooser directoryChooserOut = new DirectoryChooser();
        directoryChooserOut.setInitialDirectory(new File("/tmp/"));
        Button buttonFolderOut = new Button("Set output folder");
        buttonFolderOut.setOnAction(e -> outFolder = new File(directoryChooserOut.showDialog(stage), tfName.getText() + ".json"));
        return buttonFolderOut;
    }

    public Button getOkButton() {
        Button btn = new Button("Create");
        //action to be performed
        btn.setOnAction(e -> getConfirmationAction());
        return btn;
    }

    public boolean getConfirmationAction() {
        boolean confirm = false;
        name = tfName.getText();
        maximalArea = tfMA.getText();
        minimalArea = tfMiA.getText();
        minimalWidthContactRoad = tfWidth.getText();
        streetLane = tfSL.getText();
        streetWidth = tfSW.getText();
        laneWidth = tfLW.getText();
        blockShape = tfBS.getText();
        maxDepth = tfMD.getText();
        maxDistanceForNearestRoad = tfMDNR.getText();
        maxWidth = tfMW.getText();
        lenDriveway = tfDW.getText();
        harmonyCoeff = tfHC.getText();
        irregularityCoeff = tfN.getText();
        approxNumberParcelPerBlock = tfANPPB.getText();
        if (controlValues()) {
            confirm = true;
            switch (process) {
                case "obb":
                    profile = new ProfileUrbanFabric(name, Double.parseDouble(maximalArea), Double.parseDouble(minimalArea), Double.parseDouble(minimalWidthContactRoad),
                            Double.parseDouble(streetWidth), Integer.parseInt(streetLane), Double.parseDouble(laneWidth), Integer.parseInt(blockShape), Double.parseDouble(harmonyCoeff), Double.parseDouble(irregularityCoeff));
                    break;
                case "ss":
                    profile = new ProfileUrbanFabric(name, Double.parseDouble(minimalArea), Double.parseDouble(maxDepth), Double.parseDouble(maxDistanceForNearestRoad), Double.parseDouble(minimalWidthContactRoad),
                            Double.parseDouble(maxWidth), Double.parseDouble(laneWidth), Double.parseDouble(irregularityCoeff));
                    break;
                case "densification":
                    profile = new ProfileUrbanFabric(name, Double.parseDouble(maximalArea), Double.parseDouble(minimalArea), Double.parseDouble(minimalWidthContactRoad),
                            Double.parseDouble(lenDriveway), Double.parseDouble(harmonyCoeff), Double.parseDouble(irregularityCoeff));
                    break;
                case "all":
                case "OBBThenSS":
                    profile = new ProfileUrbanFabric(name, Double.parseDouble(maximalArea), Double.parseDouble(minimalArea),
                            Double.parseDouble(minimalWidthContactRoad), Double.parseDouble(laneWidth), Double.parseDouble(streetWidth), Integer.parseInt(streetLane),
                            Integer.parseInt(blockShape), Double.parseDouble(lenDriveway), Double.parseDouble(maxDepth),
                            Double.parseDouble(maxDistanceForNearestRoad), Double.parseDouble(maxWidth), Integer.parseInt(approxNumberParcelPerBlock),
                            Double.parseDouble(harmonyCoeff), Double.parseDouble(irregularityCoeff));
            }
            if (outFolder != null) {
                try {
                    exportToJSON();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
        return confirm;
    }

    private boolean controlValues() {
        for (String field : neededFields) {
            if ((field.equals("maximalArea") && !NumberUtils.isCreatable(maximalArea))
                    || (field.equals("minimalArea") && !NumberUtils.isCreatable(minimalArea))
                    || (field.equals("minimalWidthContactRoad") && !NumberUtils.isCreatable(minimalWidthContactRoad))
                    || (field.equals("largeStreetLevel") && !NumberUtils.isCreatable(streetLane))
                    || (field.equals("decompositionLevelWithoutStreet") && !NumberUtils.isCreatable(blockShape))
                    || (field.equals("largeStreetWidth") && !NumberUtils.isCreatable(streetWidth))
                    || (field.equals("smallStreetWidth") && !NumberUtils.isCreatable(laneWidth))
                    || (field.equals("maxDepth") && !NumberUtils.isCreatable(maxDepth))
                    || (field.equals("maxDistanceForNearestRoad") && !NumberUtils.isCreatable(maxDistanceForNearestRoad))
                    || (field.equals("maxWidth") && !NumberUtils.isCreatable(maxWidth))
                    || (field.equals("approxNumberParcelPerBlock") && !NumberUtils.isCreatable(approxNumberParcelPerBlock))
                    || (field.equals("harmonyCoeff") && !NumberUtils.isCreatable(harmonyCoeff))
                    || (field.equals("irregularityCoeff") && !NumberUtils.isCreatable(irregularityCoeff))
                    || (field.equals("lenDriveway") && !NumberUtils.isCreatable(lenDriveway))) {
                notGoodValue(field);
                return false;
            }
        }
        return true;
    }

    //minimalArea, name, minimalWidthContactRoad,largeStreetLevel,decompositionLevelWithoutStreet, largeStreetWidth, smallStreetWidth"
    public void exportToJSON() throws IOException {
        profile.exportToJSON(outFolder);
    }

    public Button buttonImport(Stage stage) {
        FileChooser fileChooserPUF = new FileChooser();
        fileChooserPUF.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
        Button buttonPUF = new Button("Import an Urban Fabric Profile");
        buttonPUF.setOnAction(e -> {
            File f = fileChooserPUF.showOpenDialog(stage);
            if (f != null) {
                try {
                    importJSON(f);
                    getUrbanProfileParams(process);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
        return buttonPUF;
    }

    public void importJSON(File f) throws IOException {
        profile = ProfileUrbanFabric.convertJSONtoProfile(f);
        tfvName = profile.getNameBuildingType();
        tfvMA = String.valueOf(profile.getMaximalArea());
        tfvMiA = String.valueOf(profile.getMinimalArea());
        tfvWidth = String.valueOf(profile.getMinimalWidthContactRoad());
        tfvSL = String.valueOf(profile.getStreetLane());
        tfvSW = String.valueOf(profile.getStreetWidth());
        tfvBS = String.valueOf(profile.getBlockShape());
        tfvLW = String.valueOf(profile.getLaneWidth());
        tfvDW = String.valueOf(profile.getDrivewayWidth());
        tfvMD = String.valueOf(profile.getMaxDepth());
        tfvMDNR = String.valueOf(profile.getMaxDistanceForNearestRoad());
        tfvMW = String.valueOf(profile.getMaxWidth());
        tfvANPPB = String.valueOf(profile.getApproxNumberParcelPerBlock());
        tfvN = String.valueOf(profile.getIrregularityCoeff());
        tfvHC = String.valueOf(profile.getHarmonyCoeff());
/* attempt to refresh the values of the fields (just copied/pasted code)
        Task<Void> tfvtMA = new Task<Void>() {
            {
                updateMessage("Initial text");
            }

            @Override
            public Void call() throws Exception {
                    if (NumberUtils.isCreatable(String.valueOf(profile.getMaximalArea())))
                        updateMessage(String.valueOf(profile.getMaximalArea()));
                    Thread.sleep(1000);
                }

        };
*/
    }
}
