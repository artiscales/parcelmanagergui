package fr.ign.artiscales.pm.gui.setParameters;

import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.pm.parcelFunction.MarkParcelAttributeFromPosition;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.geotools.data.DataStore;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class ParcelImport {

    static SimpleFeatureCollection sfcParcel;

    public static void setFields(List<String> listFields) {
        ChoiceDialog<String> dialogCodeCom = new ChoiceDialog<>(listFields.get(1), listFields);
        dialogCodeCom.setTitle("Set Default Parcel Fields ");
        dialogCodeCom.setHeaderText("");
        dialogCodeCom.setContentText("Choose the field representing the community code");
    }


    public static SimpleFeatureCollection parcelImport(File pFile, boolean checkMarkParcel) {
        try {
//            EventHandler<InputEvent> parcelName = new EventHandler<InputEvent>() {
//                @Override
//                public void handle(InputEvent inputEvent) {
//                    Tools.getCollectionName(sfcParcel);
//                }
//            };
            DataStore ds = CollecMgmt.getDataStore(pFile);
            sfcParcel = DataUtilities.collection(ds.getFeatureSource(ds.getTypeNames()[0]).getFeatures());
            setFields(sfcParcel.getSchema().getAttributeDescriptors().stream().map(x -> x.getName().toString()).collect(Collectors.toList()));
            if (checkMarkParcel && MarkParcelAttributeFromPosition.isNoParcelMarked(sfcParcel)) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("No Marked Parcels");
                alert.setHeaderText("No marked parcels have been found in the input parcel set");
                GridPane expContent = new GridPane();
                Label label = new Label("You have to set an attribute field which mark the parcels that are going to be cut."
                        + "\n\nDedicated methods could be found in the footer of the main menu."
                        + "\n\nThe current name of the filed is \"" + MarkParcelAttributeFromPosition.getMarkFieldName() + "\""
                        + " but you can change it with the\nMarkParcelAttributeFromPosition.setMarkFieldName() method."
                        + "\n\nSee also the other method in MarkParcelAttributeFromPosition class to automate the marking of parcels."
                        + "\n\n\nDo you want to mark every parcels ?\n\n");
                expContent.setMaxWidth(Double.MAX_VALUE);
                expContent.add(label, 0, 0);

                Button buttonMarkAll = new Button("Mark every parcels");
                buttonMarkAll.setOnAction(e -> {
                    sfcParcel = MarkParcelAttributeFromPosition.markAllParcel(sfcParcel);
                    System.out.println("parcels marked");
                    alert.close();
                });

                ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
                expContent.add(buttonMarkAll, 0, 1);
                alert.getButtonTypes().setAll(buttonTypeCancel);
                // Set expandable Exception into the dialog pane.
                alert.getDialogPane().setContent(expContent);
                alert.showAndWait();
            }
            ds.dispose();
        } catch (Exception ex) {
            Tools.printEx(ex);
        }
        return sfcParcel;
    }
}
