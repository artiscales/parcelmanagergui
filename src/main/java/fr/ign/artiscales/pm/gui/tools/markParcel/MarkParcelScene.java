package fr.ign.artiscales.pm.gui.tools.markParcel;

import fr.ign.artiscales.pm.gui.App;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MarkParcelScene {
    static String title = "Parcel Marking";

    public Scene getScene(Stage stage) {
        final GridPane inputGridPane = new GridPane();
        Pane rootGroup;
        stage.setTitle(title);
        Button buttonMarkParcelAreaInf = new Button("Mark parcels under a specific area");
        buttonMarkParcelAreaInf.setOnAction(e -> stage.setScene(new MarkParcelAreaInf().getScene(stage)));

        Button buttonMarkParcelAreaSup = new Button("Mark parcels above a specific area");
        buttonMarkParcelAreaSup.setOnAction(e -> stage.setScene(new MarkParcelAreaSup().getScene(stage)));

        Button buttonMarkRandomParcels = new Button("Mark random parcels");
        buttonMarkRandomParcels.setOnAction(e -> stage.setScene(new MarkParcelRandom().getScene(stage)));

        Button buttonMarkUnbuiltParcel = new Button("Mark unbuilt parcels");
        buttonMarkUnbuiltParcel.setOnAction(e -> stage.setScene(new MarkParcelUnbuilt().getScene(stage)));

        Button buttonMarkBuiltParcel = new Button("Mark built parcels");
        buttonMarkBuiltParcel.setOnAction(e -> stage.setScene(new MarkParcelBuilt().getScene(stage)));

        Button buttonMarkParcelIntersectingPolygon = new Button("Mark parcels intersecting a collection of polygon");
        buttonMarkParcelIntersectingPolygon.setOnAction(e -> stage.setScene(new MarkParcelIntersectingPolygon().getScene(stage)));

        Button buttonMarkParcelWithAttributeTable = new Button("Mark parcels with attribute table");
        buttonMarkParcelWithAttributeTable.setOnAction(e -> stage.setScene(new MarkParcelWithAttribute().getScene(stage)));

        Button buttonMarkParcelConnectedToRoad = new Button("Mark parcels connected to road");
        buttonMarkParcelConnectedToRoad.setOnAction(e -> stage.setScene(new MarkParcelConnectedToRoad().getScene(stage)));

        Button buttonMarkAllParcel = new Button("Mark every parcels");
        buttonMarkAllParcel.setOnAction(e -> stage.setScene(new MarkParcelAll().getScene(stage)));

        GridPane.setConstraints(buttonMarkParcelAreaSup, 0, 1);
        GridPane.setConstraints(buttonMarkParcelAreaInf, 0, 2);
        GridPane.setConstraints(buttonMarkParcelConnectedToRoad, 0, 3);
        GridPane.setConstraints(buttonMarkParcelIntersectingPolygon, 0, 4);
        GridPane.setConstraints(buttonMarkParcelWithAttributeTable, 0, 5);
        GridPane.setConstraints(buttonMarkBuiltParcel, 0, 6);
        GridPane.setConstraints(buttonMarkUnbuiltParcel, 0, 7);
        GridPane.setConstraints(buttonMarkAllParcel, 0, 8);
        GridPane.setConstraints(buttonMarkRandomParcels, 0, 9);
        inputGridPane.setHgap(8);
        inputGridPane.setVgap(8);
        inputGridPane.getChildren().addAll(buttonMarkParcelAreaSup, buttonMarkParcelAreaInf, buttonMarkParcelConnectedToRoad, buttonMarkParcelIntersectingPolygon,
                buttonMarkParcelWithAttributeTable, buttonMarkBuiltParcel, buttonMarkUnbuiltParcel, buttonMarkAllParcel, buttonMarkRandomParcels);

        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
        rootGroup.getChildren().add(App.returnToMain(stage));

        return new Scene(rootGroup, App.width, App.height);
    }

    public void run() {
    }


}
