package fr.ign.artiscales.pm.gui.process;

import fr.ign.artiscales.pm.division.OBBDivision;
import fr.ign.artiscales.pm.gui.tools.CreateUrbanProfile;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

public class OrientedBoundingBoxProcess extends ProcessScene {
    boolean forceStreetAccess = false;

    public OrientedBoundingBoxProcess(Stage stage) {
        super(stage);
        stage.setTitle("Oriented bounding box process");
        necessariesElements = Arrays.asList("parcel", "outputFolder");
        optionalElements = Collections.singletonList("road");
        VBox parcel = new VBox();
        parcel.getChildren().addAll(getButtonParcel(stage), new Label(parcelName));

        VBox road = new VBox();
        road.getChildren().addAll(getButtonRoad(stage), new Label("optional" + roadName));

        VBox outputFolder = new VBox();
        outputFolder.getChildren().addAll(getOutFolderButton(stage), new Label(outFolder != null ? outFolder.getName() : ""));

        String process = "obb";
        CreateUrbanProfile cub = new CreateUrbanProfile(process);
        VBox params = cub.getUrbanProfileParams(process);
        cub.setNeededFields(process);

        Button importPUF = cub.buttonImport(stage);

        CheckBox checkForceStreetAccess = new CheckBox("Force street access");
        checkForceStreetAccess.setOnAction(action -> forceStreetAccess = checkForceStreetAccess.isSelected());

        Button runButton = new Button("Run");
        runButton.setOnAction(e -> {
            if (cub.getConfirmationAction() && checkIfFilled()) {
                this.profile = cub.getProfile();
                try {
                    CollecMgmt.exportSFC(OBBDivision.splitParcels(sfcParcel, roadFile, profile, forceStreetAccess), Tools.getNewFileToSave(outFolder, "OrientedBoundingBoxDivided"));
                    Tools.getProcessAlert("Oriented bounding box process has successfully been proceeded. Find the result in the " + outFolder.getAbsolutePath() + " folder","Done");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
        rootGroup.getChildren().addAll(new Label("Geographic data to import"), parcel, road, outputFolder, params, checkForceStreetAccess, importPUF, runButton);
    }

}
