package fr.ign.artiscales.pm.gui.setParameters;

import java.io.File;

import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.tools.parameter.ProfileUrbanFabric;

public class ProfileUrbanFabricImport {
	static ProfileUrbanFabric profile;

	public static ProfileUrbanFabric getProfileUrbanFabricImport(File f) {
		try {
			profile = ProfileUrbanFabric.convertJSONtoProfile(f);
		} catch (Exception ex) {
			Tools.printEx(ex);
		}
		return profile;
	}
}
