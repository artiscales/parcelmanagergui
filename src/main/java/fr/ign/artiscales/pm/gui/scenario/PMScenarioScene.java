package fr.ign.artiscales.pm.gui.scenario;

import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.GeoDataImport;
import fr.ign.artiscales.pm.gui.tools.Tools;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.util.List;

public class PMScenarioScene extends GeoDataImport {


    public PMScenarioScene(Stage stageGoal) {
        necessariesElements = List.of("scenarioFile");
        stageGoal.setTitle("Scenario");
        Button buttonScenario = new Button("Run existing scenario");
        buttonScenario.setOnAction(e -> {
            PMScenarioScene scenario = new PMScenarioScene(stageGoal);
            stageGoal.setScene(scenario.getScene());
        });

        final GridPane inputGridPane = new GridPane();
//        GridPane.setConstraints(buttonDensificationStudy, 0, 1);
        GridPane.setConstraints(buttonScenario, 0, 0);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
//        inputGridPane.getChildren().addAll(buttonCompare, buttonDensificationStudy, buttonTestScenario);
        inputGridPane.getChildren().addAll(buttonScenario);
        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
        stageGoal.setTitle("Run a scenario");
        getParameters(stageGoal);
    }

    public void getParameters(Stage stage) {
        VBox rootF = new VBox();
        FileChooser fileChooserZone = new FileChooser();
        fileChooserZone.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("json", "*.json"));
        Button buttonRoot = new Button("Select json file");
        buttonRoot.setOnAction(e -> {
            scenarioFile = fileChooserZone.showOpenDialog(stage);
            // lastFolder = outFolder;
        });
        rootF.getChildren().addAll(new Label("Select the .json file of the scenario. Every path must be absolute. \nNo progress bar are programmed. Understand that running scenarios might take a lot of time."), buttonRoot);

        Button buttonLaunch = new Button("Launch Scenario");
        buttonLaunch.setOnAction(e -> {
//            Tools.getStartAlert("UseCase <b>Test Scenario</b>");
            run();
            Tools.getFinishAlert(outFolder, "Scenario");
        });
        final GridPane inputGridPane = new GridPane();
        GridPane.setConstraints(rootF, 0, 0);
        GridPane.setConstraints(buttonLaunch, 0, 4);
        inputGridPane.setHgap(4);
        inputGridPane.setVgap(4);
        inputGridPane.getChildren().addAll(rootF, buttonLaunch);

        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
        rootGroup.getChildren().add(App.returnToMain(stage));
    }

    protected void run() {
        if (checkIfFilled()) {
            try {
                new fr.ign.artiscales.pm.scenario.PMScenario(scenarioFile).executeStep();
            } catch (Exception e) {
                Tools.printEx(e);
                e.printStackTrace();
            }
        }
    }

    public static class MainScenario extends PMScenarioScene {

        public MainScenario(Stage stageGoal) {
            super(stageGoal);
        }

    }
}
