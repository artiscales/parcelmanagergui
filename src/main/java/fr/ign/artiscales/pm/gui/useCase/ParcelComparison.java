package fr.ign.artiscales.pm.gui.useCase;

import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.pm.usecase.CompareSimulatedWithRealParcels;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ParcelComparison extends UseCaseScene {

    static List<String> necessariesElements = Arrays.asList("parcel", "road", "profileUrbanFabric", "outputFolder");

    public ParcelComparison(Stage stageGoal) {
        super(stageGoal, necessariesElements);
        stageGoal.setTitle("Compare Simulated Parcels With Evolution Use Case");
        getParameters(stageGoal);
    }

    @Override
    protected void run() {
        if (checkIfFilled())
            try {
                CompareSimulatedWithRealParcels.compareSimulatedParcelsWithEvolutionWorkflow(rootFolder, outFolder);
            } catch (IOException e) {
                Tools.printEx(e);
                e.printStackTrace();
            }
    }
}
