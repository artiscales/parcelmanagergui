package fr.ign.artiscales.pm.gui.setParameters;

import java.io.File;

import fr.ign.artiscales.pm.gui.tools.Tools;
import org.geotools.data.DataStore;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;

import fr.ign.artiscales.tools.geoToolsFunctions.vectors.Geopackages;

public class RoadImport {
	static SimpleFeatureCollection sfcRoad;

	public static SimpleFeatureCollection roadImport(File f) {
		try {
			DataStore ds = Geopackages.getDataStore(f);
			sfcRoad = DataUtilities.collection(ds.getFeatureSource(ds.getTypeNames()[0]).getFeatures());
			ds.dispose();
		} catch (Exception ex) {
			Tools.printEx(ex);
		}
		return sfcRoad;
	}
}
