package fr.ign.artiscales.pm.gui.tools.markParcel;

import fr.ign.artiscales.pm.gui.App;
import fr.ign.artiscales.pm.gui.tools.Tools;
import fr.ign.artiscales.pm.parcelFunction.MarkParcelAttributeFromPosition;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.geotools.data.simple.SimpleFeatureCollection;

import java.io.File;
import java.util.Arrays;

public class MarkParcelBuilt extends MarkParcel {

    public Scene getScene(Stage stage) {
        MarkParcelAttributeFromPosition.setPostMark(true);
        stage.setTitle("Marking built parcels");
        necessariesElements = Arrays.asList("parcel", "outputFolder", "building");
        final GridPane inputGridPane = new GridPane();
        Pane rootGroup;
        Button parcelButton = getButtonParcel(stage, false);
        Button outButton = getOutFolderButton(stage);
        Button runButton = getRunButton();
        Button buildingButton = getButtonBuilding(stage);
        GridPane.setConstraints(parcelButton, 0, 1);
        GridPane.setConstraints(buildingButton, 0, 2);
        GridPane.setConstraints(outButton, 0, 3);
        GridPane.setConstraints(reuseMarking, 0, 5);
        GridPane.setConstraints(runButton, 0, 7);
        inputGridPane.setHgap(8);
        inputGridPane.setVgap(8);
        inputGridPane.getChildren().addAll(parcelButton, buildingButton, outButton, reuseMarking, runButton);
        rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
        rootGroup.getChildren().add(App.returnToMain(stage));

        return new Scene(rootGroup, App.width, App.height);
    }

    @Override
    public void run() {
        if (checkIfFilled())
            try {
                File fileOut = Tools.getNewFileToSave(outFolder, parcelName + "BuiltMark.gpkg");
                SimpleFeatureCollection markedP = MarkParcelAttributeFromPosition.markBuiltParcel(reuseMarking.isSelected() ? sfcParcel : MarkParcelAttributeFromPosition.resetMarkingField(sfcParcel), buildingFile);
                CollecMgmt.exportSFC(markedP, fileOut);
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
                alert2.setContentText("Process of marking built parcels done. "
                        + MarkParcelAttributeFromPosition.countMarkedParcels(markedP) + " parcels have been marked. Find the result in the " + fileOut + " file");
                alert2.setHeaderText("Success");
                alert2.show();
            } catch (Exception e) {
                Tools.printEx(e);
                e.printStackTrace();
            }
    }
}
