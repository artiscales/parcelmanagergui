package fr.ign.artiscales.pm.gui.useCase;

import fr.ign.artiscales.pm.gui.tools.Tools;
import javafx.stage.Stage;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class TestScenario extends UseCaseScene {

    static List<String> necessariesElements = Arrays.asList("rootFolder", "outputFolder");

    public TestScenario(Stage stageGoal) {
        super(stageGoal, necessariesElements);
        stageGoal.setTitle("Basic scenario of the article");
        getParameters(stageGoal);
    }

    @Override
    protected void run() {
        if (checkIfFilled()) {
            try {
                fr.ign.artiscales.pm.usecase.TestScenario.doTestScenario(outFolder, new File(rootFolder, "InputData"));
            } catch (Exception e) {
                Tools.printEx(e);
                e.printStackTrace();
            }
        }
    }
}
